import { EAction, SetMessagePayload } from "./types";

export const setMessage = (message: SetMessagePayload) => ({
  type: EAction.SET_MESSAGE,
  payload: message,
});

export const clearMessage = {
  type: EAction.CLEAR_MESSAGE,
};
import React from "react";
import * as UserService from '../../../services/user.service';

import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

const antIcon = <LoadingOutlined style={{ fontSize: 36 }} spin />;

const DefaultPage = () => {
  const [content, setContent] = React.useState("");
  const [loaded, setLoaded] = React.useState(false);

  React.useEffect(() => {
    setLoaded(false);

    UserService.getPublicContent().then(
      (response) => {
        setContent('Доступ разрешен');
      },
      ).catch(() => {
        setContent("Ошибка запроса");
      }).finally(() => {
        setLoaded(true);
      });
  }, []);

  return (
    <div>
      { loaded ? <span>{ content }</span> : <Spin indicator={antIcon} /> }
    </div>
  );
};

export default DefaultPage;

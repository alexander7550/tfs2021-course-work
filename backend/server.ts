/* eslint-disable @typescript-eslint/no-explicit-any */
import express, { Request, Response, NextFunction } from "express";
import cors from "cors"

import { db } from "./src/models";

import { addAuthMethods } from './src/routes/auth.routes';
import { addUserRoutes  } from './src/routes/user.routes';
import { defaultCommentsInit, defaultFeedbacksInit, defaultPostsInit, defaultRolesInit, defaultUsersInit } from "./src/models/initDefaultDb";
import logger from "./src/utils/logger";

const app = express();

const corsOptions = {
  origin: "http://localhost:3000"
};

app.use(cors(corsOptions)); // чтобы локально дебажить

app.use(express.static('./../frontend/build/'));

// парсим content-type - application/json
app.use(express.json());

// парсим content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

addAuthMethods(app);
addUserRoutes(app);

app.get("*", (req: Request, res: Response) => {
  res.sendFile('index.html', { root: './../frontend/build/' });
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  console.error(err.stack);
  res.status(500).send('Something broken!');
});

const PORT = process.env.PORT || 3333;
app.listen(PORT, () => {
  logger.info(`Server is running on port ${PORT}.`);
});

// сбрасываем базу данных
db.sequelize.sync({force: true}).then(() => {
  logger.info('Пересоздаем db.');
  defaultRolesInit();
  defaultUsersInit();
  defaultPostsInit();
  defaultCommentsInit();
  defaultFeedbacksInit();
});

// не сбрасываем базу а просто загружаем
// db.sequelize.sync();
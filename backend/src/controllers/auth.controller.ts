/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import { db } from "../models";
import { Request, Response } from "express";
import { EStatusCode, RequestStatus } from "../types";
import * as config from "../config/auth.config";
import logger from "../utils/logger";
import { validationResult } from 'express-validator';

const User = db.user;

const signup = async (req: Request, res: Response) => {
  let bodyUsername: string = req.body.username;
  let bodyEmail: string = req.body.email;
  const bodyPassword: string = req.body.password;
  
  if (!bodyUsername) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "username" }
    res.status(400).send(status);
    logger.debug(`[SignUp] Ошибка регистрации! Не передан username!`);
    return;
  }

  if (!bodyEmail) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "email" }
    res.status(400).send(status);
    logger.debug(`[SignUp] Ошибка регистрации! Не передан email!`);
    return;
  }

  if (!bodyPassword) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "password" }
    res.status(400).send(status);
    logger.debug(`[SignUp] Ошибка регистрации! Не передан password!`);
    return;
  }

  bodyUsername = bodyUsername.toLowerCase();
  bodyEmail = bodyEmail.toLowerCase();

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const firstError = errors.array()[0];

    logger.debug(`[SignUp] Ошибка регистрации! Email невалидный!`);

    const status: RequestStatus = { status_code: EStatusCode.InvalidParamValue, param: firstError.param }
    res.status(400).send(status);
    return;
  }

  try {
    const userFound = await User.findOne({
      where: {
        username: bodyUsername
      }
    });

    if (userFound) { // логин уже занят 
      const status: RequestStatus = {
        status_code: EStatusCode.UsernameTaken
      }

      res.status(400).send(status);
      logger.debug(`[SignUp] Ошибка регистрации! Логин ${bodyUsername} уже занят!`);
      return;
    }

    const emailFound = await User.findOne({
      where: {
        email: bodyEmail
      }
    });

    if (emailFound) {
      const status: RequestStatus = {
        status_code: EStatusCode.EmailTaken
      }

      res.status(400).send(status);
      logger.debug(`[SignUp] Ошибка регистрации! Email ${bodyEmail} уже занят!`);
      return;
    }

    const newUser = {
      username: bodyUsername,
      email: bodyEmail,
      password: bcrypt.hashSync(bodyPassword, 8)
    }

    // Все проверки пройдены, создаем учетку
    const createdUser: any = await User.create(newUser);

    createdUser.setRoles([1]).then(() => {
      const status: RequestStatus = {
        status_code: EStatusCode.OK,
        result: {
          username: newUser.username,
          email: newUser.email
        }
      }

      res.status(200).send(status);
      logger.debug(`[SignUp] Пользователь ${bodyUsername} успешно зарегистрирован!`);
      return;
    });
  } 
  catch (ex) {
    logger.error(`[SignUp] Исключение: ${ex}.`);
    res.status(500).send();
  }
};

const signin = async (req: Request, res: Response) => {
  let bodyUsername: string = req.body.username;
  const bodyPassword: string = req.body.password;

  if (!bodyUsername) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "username" }
    res.status(400).send(status);
    logger.debug(`[SignIn] Ошибка авторизации! Не передан username!`);
    return;
  }

  if (!bodyPassword) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "password" }
    res.status(400).send(status);
    logger.debug(`[SignIn] Ошибка авторизации! Не передан password!`);
    return;
  }

  bodyUsername = bodyUsername.toLowerCase();

  try {
    const foundUser: any = await User.findOne({
      where: {
        username: bodyUsername
      }
    });

    const invalidCredetailsStatus: RequestStatus = {
      status_code: EStatusCode.InvalidUsernameOrPassword
    }

    if (!foundUser) {
      res.status(401).send(invalidCredetailsStatus);
      logger.debug(`[SignIn] Ошибка авторизации! Введен неверный логин: "${bodyUsername}"!`);
      return;
    }

    const passwordIsValid = bcrypt.compareSync(
      bodyPassword,
      foundUser.password
    );

    if (!passwordIsValid) {
      res.status(401).send(invalidCredetailsStatus);
      logger.debug(`[SignIn] Ошибка авторизации! Введен неверный пароль!`);
      return;
    }

    const token = jwt.sign({ id: foundUser.id }, config.secret, {
      //expiresIn: 86400 // 24 hours
      expiresIn: 21600
    });

    const authorities = new Array<string>();

    const roles: any = await foundUser.getRoles();

    for (let i = 0; i < roles.length; i++) {
      authorities.push("ROLE_" + roles[i].name.toUpperCase());
    }

    const status: RequestStatus = {
      status_code: EStatusCode.OK,
      result: {
        id: foundUser.id,
        username: foundUser.username,
        email: foundUser.email,
        roles: authorities,
        accessToken: token,
      }
    }

    res.status(200).send(status);
    logger.debug(`[SignUp] Пользователь ${bodyUsername} успешно авторизован!`);
    return;

  }
  catch (ex) {
    logger.error(`[SignUp] Исключение: ${ex}.`);
    res.status(500).send();
  }
};

export {
  signin,
  signup
}
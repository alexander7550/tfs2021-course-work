/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request, Response } from "express";

import { Op } from "sequelize";
import { raspisanie, raspisanieKursi } from "../config/db.config";
import { db } from "../models";
import { CommentType } from "../models/comments.model";
import { FeedbackType } from "../models/feedback.model";

import { PostType } from "../models/posts.model";
import { EStatusCode, RequestStatus } from "../types";
import { getCurrentUnixTimeSeconds } from "../utils";
import logger from "../utils/logger";

const post = db.post;
const comment = db.comment;
const feedback = db.feedback;

const allAccess = (req: Request, res: Response): void => {
  res.status(200).send();
};

const userBoard = (req: Request, res: Response): void => {
  res.status(200).send();
};

const adminBoard = (req: Request, res: Response): void => {
  res.status(200).send();
};

const moderatorBoard = (req: Request, res: Response): void => {
  res.status(200).send();
};

const addNewPost = (req: Request, res: Response): void => {
  const bodyTitle = req.body.title;
  const bodyHtml = req.body.html;
  const bodyPosterLink = req.body.posterLink;
  const bodyPreviewText = req.body.previewText;

  if (!bodyTitle) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "title" }
    res.status(400).send(status);
    logger.debug(`[AddNewPost] Ошибка добавления поста! Не передан title!`);
    return;
  }

  if (!bodyHtml) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "html" }
    res.status(400).send(status);
    logger.debug(`[AddNewPost] Ошибка добавления поста! Не передан html!`);
    return;
  }

  if (!bodyPosterLink) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "posterLink" }
    res.status(400).send(status);
    logger.debug(`[AddNewPost] Ошибка добавления поста! Не передан posterLink!`);
    return;
  }

  if (!bodyPreviewText) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "previewText" }
    res.status(400).send(status);
    logger.debug(`[AddNewPost] Ошибка добавления поста! Не передан previewText!`);
    return;
  }

  const newPost: PostType = {
    title: bodyTitle,
    html: bodyHtml,
    posterLink: bodyPosterLink,
    unixDate: getCurrentUnixTimeSeconds(),
    previewText: bodyPreviewText
  }

  post.create(newPost).then((p: any) => {
    const status: RequestStatus = { 
      status_code: EStatusCode.OK,
      result: {
        id: p.id
      }
    };

    res.status(200).send(status);
    logger.debug(`[AddNewPost] Пост успешно создан!`);

  }).catch(() => {
    res.status(500).send();
    logger.error(`[AddNewPost] Ошибка!`);

  });
}

const addNewComment = (req: Request, res: Response): void => {
  const bodyPostID = req.body.postId;
  const bodyCommentText = req.body.text;
  const bodyAuthorName = req.body.authorName;

  if (!bodyPostID) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "postId" }
    res.status(400).send(status);
    logger.debug(`[AddNewComment] Ошибка добавления поста! Не передан postId!`);
    return;
  }

  if (!bodyCommentText) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "text" }
    res.status(400).send(status);
    logger.debug(`[AddNewComment] Ошибка добавления поста! Не передан commentText!`);
    return;
  }

  if (!bodyAuthorName) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "authorName" }
    res.status(400).send(status);
    logger.debug(`[AddNewComment] Ошибка добавления поста! Не передан authorName!`);
    return;
  }

  const newComment: CommentType = {
    postId: bodyPostID,
    text: bodyCommentText,
    authorName: bodyAuthorName,
    authorAvatar: '',
    unixDate: getCurrentUnixTimeSeconds(),
  }

  comment.create(newComment).then(() => {
    const status: RequestStatus = { 
      status_code: EStatusCode.OK,
      result: newComment
    }
    res.status(200).send(status);
    logger.debug(`[AddNewComment] Комментарий успешно добавлен!`);
    
  }).catch(() => {
    res.status(500).send();
    logger.error(`[AddNewComment] Ошибка при добавлении комментария!`);
  });
}

const findCommends = (id:any) => {
  return new Promise<any[]>(resolve => {
    comment.findAll({ 
      where: {
        postId: {
          [Op.eq]: id
        }
      } 
    }).then((comments: any) => {
      const arr = comments.map((x: any) => (
        {
          id: x.id,
          text: x.text,
          authorName: x.authorName,
          authorAvatar: x.authorAvatar,
          unixDate: x.unixDate
        }));

      resolve(arr);
    }).catch(() => {
      resolve([]);
    });
  });
}

const getListNews = (req: Request, res: Response): void => {
  post.findAll().then(async (posts) => {

    const postsDiff: PostType[] = await Promise.all(posts.map(async (x: any) => {      
      return {
        id: x.id,
        title: x.title,
        posterLink: x.posterLink,
        unixDate: x.unixDate,
        previewText: x.previewText,
      }
    }));

    const status: RequestStatus = { 
      status_code: EStatusCode.OK,
      result: postsDiff.reverse() 
    }
    res.status(200).send(status);
  }).catch(() => {
    logger.error(`[GetListNews] Ошибка!`);
    res.status(500).send();
  });
}

const getSelectedNews = (req: Request, res: Response): void => {
  const bodyId = req.query.id;
  
  if (!bodyId) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "id" }
    res.status(400).send(status);
    return;
  }

  post.findOne({
    where: {
      id: bodyId
    }
  }).then(async (x: any) => {
    const comments = await findCommends(x.id);

    const status: RequestStatus = {
      status_code: EStatusCode.OK,
      result: {
        id: x.id,
        title: x.title,
        html: x.html,
        posterLink: x.posterLink,
        unixDate: x.unixDate,
        previewText: x.previewText,
        comments: comments.reverse()
      }
    }

    res.status(200).send(status);

  }).catch(() => {
    res.status(500).send();
  });
}

const getLatestNews = (req: Request, res: Response): void => {
  post.findAll({ order: [['id', 'DESC']], limit: 3 }).then(posts => {
    const postsDiff: PostType[] = posts.map((x: any) => ({
      id: x.id,
      title: x.title,
      html: '',
      posterLink: x.posterLink,
      unixDate: x.unixDate,
      previewText: x.previewText
    }));

    const status: RequestStatus = { 
      status_code: EStatusCode.OK,
      result: postsDiff 
    }
    res.status(200).send(status);
  }).catch(() => {
    res.status(500).send();
  });
}

const getScheduleCourses = (req: Request, res: Response): void => {
  const status: RequestStatus = {
    status_code: EStatusCode.OK,
    result: raspisanieKursi
  }
  res.status(200).send(status);
}

const getScheduleCoursePairs = (req: Request, res: Response): void => { 
  const bodyId = req.query.id;
  
  if (!bodyId) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "id" }
    res.status(400).send(status);
    return;
  }

  const index = Number(bodyId)-1;

  const groupsLength = Object.keys(raspisanie).length - 1;

  if (index >= 0 && index < groupsLength) {
    const status: RequestStatus = { 
      status_code: EStatusCode.OK,
      result: raspisanie[index] 
    }
    res.status(200).send(status);
  } else {
    res.status(500).send();
  }
}

const sendFeedback = (req: Request, res: Response): void => {
  const bodyAuthorName = req.body.authorName;
  const bodyAuthorEmail = req.body.authorEmail;
  const bodyText = req.body.text;

  if (!bodyAuthorName) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "authorName" }
    res.status(400).send(status);
    logger.debug(`[SendFeedback] Ошибка добавления поста! Не передан authorName!`);
    return;
  }
  
  if (!bodyAuthorEmail) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "authorEmail" }
    res.status(400).send(status);
    logger.debug(`[SendFeedback] Ошибка добавления поста! Не передан authorEmail!`);
    return;
  }

  if (!bodyText) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "text" }
    res.status(400).send(status);
    logger.debug(`[SendFeedback] Ошибка добавления поста! Не передан text!`);
    return;
  }

  const newFeedback: FeedbackType = {
    authorName: bodyAuthorName,
    authorEmail: bodyAuthorEmail,
    text: bodyText,
    unixDate: getCurrentUnixTimeSeconds(),
  }

  feedback.create(newFeedback).then(() => {
    const status: RequestStatus = { 
      status_code: EStatusCode.OK,
      result: newFeedback
    }
    res.status(200).send(status);
    logger.debug(`[SendFeedback] Feedback успешно добавлен!`);
    
  }).catch(() => {
    res.status(500).send();
    logger.error(`[SendFeedback] Ошибка при добавлении feedback!`);
  });
}

const getFeedbacks = (req: Request, res: Response): void => {
  feedback.findAll().then(async (result) => {

    const feedbacks = result.map((x: any) => ({
      id: x.id,
      authorName: x.authorName,
      authorEmail: x.authorEmail,
      text: x.text,
      unixDate: x.unixDate 
    }));

    const status: RequestStatus = { 
      status_code: EStatusCode.OK,
      result: feedbacks
    }
    res.status(200).send(status);
  }).catch(() => {
    logger.error(`[GetFeedbacks] Ошибка!`);
    res.status(500).send();
  });
}

const deleteArticle = (req: Request, res: Response): void => { 
  const bodyId = req.query.id;
  
  if (!bodyId) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "id" }
    res.status(400).send(status);
    return;
  }

  post.destroy({
    where: {
      id: {
        [Op.eq]: bodyId
      }
    } 
  }).then(() => {
    res.status(200).send({ 
      status_code: EStatusCode.OK,
    });
  }).catch(() => {
    res.status(500).send();
  })
}

const deleteComment = (req: Request, res: Response): void => {
  const bodyId = req.query.id;
  
  if (!bodyId) {
    const status: RequestStatus = { status_code: EStatusCode.MissingParam, param: "id" }
    res.status(400).send(status);
    return;
  }

  comment.destroy({
    where: {
      id: {
        [Op.eq]: bodyId
      }
    } 
  }).then(() => {
    res.status(200).send({ 
      status_code: EStatusCode.OK,
    });
  }).catch(() => {
    res.status(500).send();
  })
}

const adminGetArticles = (req: Request, res: Response): void => {
  post.findAll().then(async (posts) => {
    const postsDiff: PostType[] = await Promise.all(posts.map(async (x: any) => {      
      return {
        id: x.id,
        title: x.title,
        posterLink: x.posterLink,
        unixDate: x.unixDate,
        previewText: x.previewText,
        comments: await findCommends(x.id)
      }
    }));

    const status: RequestStatus = { 
      status_code: EStatusCode.OK,
      result: postsDiff.reverse() 
    }
    res.status(200).send(status);
  }).catch(() => {
    logger.error(`[GetListNews] Ошибка!`);
    res.status(500).send();
  });
}

export {
  allAccess,
  userBoard,
  adminBoard,
  moderatorBoard,
  addNewPost,
  addNewComment,
  getListNews,
  getSelectedNews,
  getLatestNews,
  getScheduleCourses,
  getScheduleCoursePairs,
  sendFeedback,
  getFeedbacks,
  deleteArticle,
  deleteComment,
  adminGetArticles
}



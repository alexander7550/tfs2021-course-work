/* eslint-disable @typescript-eslint/no-explicit-any */
import jwt from "jsonwebtoken";

import * as config from "./../config/auth.config";
import { db } from './../models';

import { Request, Response, NextFunction } from "express";
import { EStatusCode, RequestStatus } from "../types";

const User = db.user;

interface ExtendedRequest extends Request {
  userId: number
}

const verifyToken = (req: ExtendedRequest, res: Response, next: NextFunction): any => {
  const token = req.headers["x-access-token"];

  if (!token) {
    const status: RequestStatus = {
      status_code: EStatusCode.MissingToken
    }

    return res.status(403).send(status);
  }

  jwt.verify(token.toString(), config.secret, (err, decoded: any) => {
    if (err) {
      const status: RequestStatus = {
        status_code: EStatusCode.InvalidToken
      };
      return res.status(401).send(status);
    }
    req.userId = decoded.id;
    next();
  });
};

const isAdmin = (req: ExtendedRequest, res: Response, next: NextFunction): any => {
  User.findByPk(req.userId).then((user: any) => {
    user.getRoles().then((roles: any) => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === "admin") {
          next();
          return;
        }
      }

      res.status(403).send();
      return;
    });
  });
};

const isModerator = (req: ExtendedRequest, res: Response, next: NextFunction): any => {
  User.findByPk(req.userId).then((user: any) => {
    user.getRoles().then((roles: any) => {
      for (let i = 0; i < roles.length; i++) {
        const roleName = roles[i].name;
        if (roleName === "moderator") {
          next();
          return;
        }
      }

      res.status(403).send();
    });
  });
};

const isModeratorOrAdmin = (req: ExtendedRequest, res: Response, next: NextFunction): any => {
  User.findByPk(req.userId).then((user: any) => {
    user.getRoles().then((roles: any) => {
      for (let i = 0; i < roles.length; i++) {
        if (roles[i].name === "moderator" || 
            roles[i].name === "admin") 
        {
          next();
          return;
        }
      }

      res.status(403).send();
    });
  });
};

const authJwt = {
  verifyToken,
  isAdmin,
  isModerator,
  isModeratorOrAdmin
};

export {
  authJwt
}

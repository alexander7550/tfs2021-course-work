/* eslint-disable @typescript-eslint/no-explicit-any */

const getKursNames = (raspisanie: Record<string, any>): any => {
  // any, потому что ide ругается
  const groupObj: any = { };

  let index = 1;

  // группируем по курсу
  for(const key in raspisanie) {
    const val = raspisanie[key];

    const kurs: string = val.KursName;
    const group: string = val.GroupName;

    if (kurs === undefined) continue;

    if (!(kurs in groupObj)) {
      groupObj[kurs] = [];
    }

    groupObj[kurs].push({ groupName: group, index });
    index += 1;
  }

  const result = new Array<any>();

  for(const key in groupObj) {
    result.push({ kursName: key, groups: groupObj[key] })
  }

  return result;
}

const getCurrentUnixTimeSeconds = (): number => {
  return Math.floor(+new Date() / 1000);
}

export {
  getKursNames,
  getCurrentUnixTimeSeconds
}
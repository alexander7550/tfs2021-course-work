import winston from 'winston';

winston.addColors({
  error: 'red',
  warn: 'yellow',
  info: 'white',
  debug: 'gray'
});

const myFormat = winston.format.combine(
  winston.format.timestamp({format:'YY-MM-DD HH:mm:ss'}),
  winston.format.printf(info => `[${[info.timestamp]}] ${info.level.toUpperCase()}: ${info.message}`),
);

const logger = winston.createLogger({
  level: 'info',
  //level: 'debug',
  format: myFormat,
  transports: [
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' }),
  ],
});


if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.combine(myFormat, winston.format.colorize({ all: true}))
  }));
}

export default logger;
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import * as controller from "../controllers/auth.controller";

import { Express, NextFunction, Request, Response } from "express";

import { body } from 'express-validator';

const addAuthMethods = (app: Express) => {
  app.use((req: Request, res: Response, next: NextFunction) => {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.post(
    "/api/auth/signup",
    [body('email').isEmail()],
    controller.signup
  );

  app.post("/api/auth/signin", controller.signin);
};

export {
  addAuthMethods
}
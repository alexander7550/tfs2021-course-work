/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { authJwt } from "../middleware";
import * as controller from "../controllers/user.controller";

import { NextFunction, Request, Response } from "express";

const addUserRoutes = (app: any) => {
  app.use(function(req: Request, res: Response, next: NextFunction) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/all", controller.allAccess);

  app.get(
    "/api/test/user",
    [authJwt.verifyToken],
    controller.userBoard
  );

  app.get(
    "/api/test/mod",
    [authJwt.verifyToken, authJwt.isModeratorOrAdmin],
    controller.moderatorBoard
  );

  app.get(
    "/api/test/admin",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminBoard
  );

  app.post(
    "/api/addPost",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.addNewPost
  );

  app.post(
    "/api/addComment",
    [authJwt.verifyToken],
    controller.addNewComment
  );

  app.get(
    '/api/getListNews',
    controller.getListNews
  );

  app.get(
    '/api/getSelectedNews',
    controller.getSelectedNews
  );

  app.get(
    '/api/getLatestNews',
    controller.getLatestNews
  );

  app.get(
    '/api/getScheduleCourses',
    controller.getScheduleCourses
  );

  app.get(
    '/api/getScheduleCoursePairs',
    controller.getScheduleCoursePairs
  );

  app.post(
    '/api/sendFeedback',
    controller.sendFeedback
  );

  app.get(
    "/api/adminka/getFeedbacks",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.getFeedbacks
  );

  app.get(
    "/api/adminka/deleteArticle",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.deleteArticle
  );

  app.get(
    "/api/adminka/deleteComment",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.deleteComment
  );

  app.get(
    "/api/adminka/getArticles",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminGetArticles
  );
};

export {
  addUserRoutes
}
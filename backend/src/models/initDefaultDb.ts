/* eslint-disable @typescript-eslint/no-explicit-any */
import { db } from ".";
import { PostType } from "./posts.model";
import bcrypt from "bcryptjs";
import { CommentType } from "./comments.model";
import { FeedbackType } from "./feedback.model";

const defaultRolesInit = (): void => {
  db.role.create({
    id: 1,
    name: "user"
  });
 
  db.role.create({
    id: 2,
    name: "moderator"
  });
 
  db.role.create({
    id: 3,
    name: "admin"
  });
}

const defaultUsersInit = (): void => { 
  db.user.create({
    username: 'admin',
    email: 'admin@domain.com',
    password: bcrypt.hashSync('tfs2021', 8),
    roles: ["admin"]
  }).then((user: any) => {
    user.setRoles([3]);
  });
  
  db.user.create({
    username: 'mod',
    email: 'mod@domain.com',
    password: bcrypt.hashSync('tfs2021', 8),
    roles: ["moderator"]
  }).then((user: any) => {
    user.setRoles([2]);
  });

  db.user.create({
    username: 'user',
    email: 'test@domain.com',
    password: bcrypt.hashSync('tfs2021', 8),
    roles: ["user"]
  }).then((user: any) => {
    user.setRoles([1]);
  });
}

const defaultPostsInit = (): void => {
  const post1: PostType = {
    id: 1,
    title: '27 марта 2021г. в УрТИСИ СибГУТИ прошел День открытых дверей.',
    posterLink: 'https://static.posohin.com/posts/3.png',
    html: "<p>Гостей приветствовала Директор института. Елена Александровна рассказала об истории и основных преимуществах учебного заведения. Далее, будущим абитуриентам и их родителям презентовали специальности, направления и профили всех ступеней обучения, порядок и особенности приемной кампании в 2021 году.</p>\n<p>Выпускающие кафедры провели экскурсии по лабораториям и мастер-классы по сварке оптического волокна, организации доступа к сети Internet и созданию игр с помощью языков визуального программирован….</p>\n<p>После дня открытых дверей на почту приемной комиссии УрТИСИ СибГУТИ приходят письма с благодарностями за полезное и интересное мероприятие, которое помогло окончательно определиться с выбором учебного заведения.</p>\n<p></p>\n<img src=\"https://static.posohin.com/posts/post3_gosti/image1.jpeg\" alt=\"undefined\" style=\"height: auto;width: 500px\"/>\n<p></p>\n<img src=\"https://static.posohin.com/posts/post3_gosti/image2.jpeg\" alt=\"undefined\" style=\"height: auto;width: 500px\"/>\n<p></p>\n",
    unixDate: 1619099187,
    previewText: 'Гостей приветствовала Директор института. Елена Александровна рассказала  об истории и основных преимуществах...'
  }

  const post2: PostType = {
    id: 2,
    title: 'УрТИСИ СибГУТИ провел День науки в "МАОУ гимназия №2"',
    posterLink: 'https://static.posohin.com/posts/1.png',
    html: '<p>12 апреля День космонавтики и Международный день  полёта человека в космос.  60 лет назад Юрий Гагарин стал первым человеком, совершившим полет  вокруг Земли, что открыло новую главу в освоении человечеством  космического пространства.</p><p>В этот день УрТИСИ СибГУТИ принял участие в проведении Дня науки,  посвященного теме космоса, в стенах гимназии №2. Представители кафедр  «МЭС», «ИСТ» и «ИТиМС» провели занятия в старших классах  технологического профиля. Ребятам рассказали о сфере It, областях  применения технологий и разных языков программирования, представили  мастер-классы по настройке и подключению беспроводной сети, сварке  оптического волокна. Гимназисты убедились, что не только исследование и  освоение космоса невозможно без инфокоммуникационных технологий и связи,  но и жизнь обычно школьника основательно ими пронизана.</p><p>В результате мероприятия с Гимназией №2 заключено соглашение о сотрудничестве.</p><p>Запланирована совместная профориентационная работа и  подготовка ребят к школьным научно-практическим конференциям по  профильной для института тематике.</p><p>Намечено развитие темы профессиональной подготовки  школьников JuniorSkills на планируемой к открытию в институте площадки  WorldSkills.</p><div style="text-align:none;"><img src="https://static.posohin.com/posts/post2_school/image1.jpg" alt="undefined" style="height: auto;width: 500px"/></div><p></p><img src="https://static.posohin.com/posts/post2_school/image2.jpg" alt="undefined" style="height: auto;width: 500px"/><p></p>',
    unixDate: 1619099187,
    previewText: '12 апреля День космонавтики и Международный день  полёта человека в космос.  60 лет назад Юрий Гагарин стал первым...'
  }

  const post3: PostType = {
    id: 3,
    title: 'УРТИСИ СИБГУТИ посетила делегация из филиала ФГУП «Главный радиочастотный центр»',
    posterLink: 'https://static.posohin.com/posts/2.png',
    html: '<p>В рамках заключенного партнерского соглашения, 31 марта 2021 года  состоялся визит в УрТИСИ СибГУТИ представителей ФГУП «ГРЧЦ» в Уральском  федеральном округе.</p><p>Директор филиала Евгений Александрович Минаев, заместитель директора  Сергей Владимирович Анучин и начальник отдела по работе с персоналом  Юлия Валерьевна Филиппова познакомились с образовательной деятельностью и  перечнем образовательных программ УрТИСИ СибГУТИ. Для гостей провели  экскурсию по институту. Заведующие выпускающих кафедр рассказали о  работе и планах развития кафедр, продемонстрировали лаборатории и  оборудование.</p><p>Представители предприятия-партнера описали возможности для прохождения  производственной практики студентов и трудоустройства выпускников УрТИСИ  СибГУТИ.</p><p>В результате встречи, между УрТИСИ СибГУТИ и филиалом ФГУП «ГРЧЦ» был  подписан договор о практической подготовке обучающихся. А также,  планируется совместное создание учебной лаборатории.</p>',
    unixDate: 1619099187,
    previewText: 'В рамках заключенного партнерского соглашения, 31 марта 2021 года  состоялся визит в УрТИСИ СибГУТИ представителей...'
  }

  db.post.create(post1);
  db.post.create(post2);
  db.post.create(post3);
}

const defaultCommentsInit = (): void => {
  const comment1: CommentType = {
    postId: 2,
    text: 'Спасибо! Приходите к нам ещё!',
    authorName: 'user',
    authorAvatar: '',
    unixDate: 1619099187
  };

  const comment2: CommentType = {
    postId: 1,
    text: 'Спасибо, помогли определиться с вузом для поступления!',
    authorName: 'user',
    authorAvatar: '',
    unixDate: 1619099187
  };

  db.comment.create(comment1);
  db.comment.create(comment2);
}

const defaultFeedbacksInit = (): void => {
  const feedback1: FeedbackType = {
    authorEmail: 'myemail@yandex.ru',
    authorName: 'Александр',
    text: 'Здравствуйте. Хочу поступить в ваш вуз. Куда подвать документы?',
    unixDate: 1620655532,
  }

  db.feedback.create(feedback1);
}

export {
  defaultRolesInit,
  defaultUsersInit,
  defaultPostsInit,
  defaultCommentsInit,
  defaultFeedbacksInit
}
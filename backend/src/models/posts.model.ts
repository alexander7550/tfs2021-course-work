/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Sequelize, DataTypes } from "sequelize";

const postModel = (sequelize: Sequelize) => {
  return sequelize.define("posts", {
    title: {
      type: DataTypes.STRING
    },
    html: {
      type: DataTypes.TEXT
    },
    posterLink: {
      type: DataTypes.STRING
    },
    unixDate: {
      type: DataTypes.BIGINT
    },
    previewText: {
      type: DataTypes.STRING
    }
  });
}

export default postModel;

export interface PostType {
  id?: number,
  title: string,
  html?: string,
  posterLink: string,
  unixDate: number,
  previewText: string
}
/* eslint-disable @typescript-eslint/no-explicit-any */
import * as config from './../config/db.config';

import { Sequelize } from "sequelize";

import userModel from './user.model';
import roleModel from './role.model';
import postModel from './posts.model';

import logger from '../utils/logger';
import commentModel from './comments.model';
import feedbackModel from './feedback.model';

const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    operatorsAliases: undefined,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    },
    logging: msg => logger.debug(msg),
  }
);

const db = {
  Sequelize,
  sequelize,
  user: userModel(sequelize),
  role: roleModel(sequelize),
  post: postModel(sequelize),
  comment: commentModel(sequelize),
  feedback: feedbackModel(sequelize),
  ROLES: ["user", "admin", "moderator"]
};

db.role.belongsToMany(db.user, {
  through: "user_roles",
  foreignKey: "roleId",
  otherKey: "userId"
});
db.user.belongsToMany(db.role, {
  through: "user_roles",
  foreignKey: "userId",
  otherKey: "roleId"
});

export {
  db
}
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Sequelize, DataTypes } from "sequelize";

const roleModel = (sequelize: Sequelize) => {
  return sequelize.define("roles", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING
    }
  });
}

export default roleModel;
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Sequelize, DataTypes } from "sequelize";

const userModel = (sequelize: Sequelize) => {
  return sequelize.define("users", {
    username: {
      type: DataTypes.STRING
    },
    email: {
      type: DataTypes.STRING
    },
    password: {
      type: DataTypes.STRING
    }
  });
}

export default userModel;
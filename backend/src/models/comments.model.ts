/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Sequelize, DataTypes } from "sequelize";

const commentModel = (sequelize: Sequelize) => {
  return sequelize.define("comments", {
    postId: {
      type: DataTypes.INTEGER
    },
    text: {
      type: DataTypes.STRING
    },
    authorName: {
      type: DataTypes.STRING
    },
    authorAvatar: {
      type: DataTypes.STRING
    },
    unixDate: {
      type: DataTypes.BIGINT
    }
  });
}

export default commentModel;

export interface CommentType {
  postId: number,
  text: string,
  authorName: string,
  authorAvatar: string,
  unixDate: number
}
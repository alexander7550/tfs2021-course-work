/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Sequelize, DataTypes } from "sequelize";

const feedbackModel = (sequelize: Sequelize) => {
  return sequelize.define("feedback", {
    authorName: {
      type: DataTypes.STRING
    },
    authorEmail: {
      type: DataTypes.STRING
    },
    text: {
      type: DataTypes.STRING
    },
    unixDate: {
      type: DataTypes.BIGINT
    }
  });
}

export default feedbackModel;

export interface FeedbackType {
  authorName: string,
  authorEmail: string,
  text: string,
  unixDate: number
}
/* eslint-disable @typescript-eslint/no-explicit-any */
import { getKursNames } from '../utils';
import * as raspisanie from './../raspisanie.json';

// в этом обьекте будет массив групп сгруппированных по курсу
const raspisanieKursi = getKursNames(raspisanie);

// чтобы можно было запускать и дома и на сервере
const isHome = process.argv.slice(2).includes("-home");

const HOST = "localhost";
const USER = isHome ? "root" : "admin_project";
const PASSWORD = isHome ? "12345678" : "VphlmRm2PX";
const DB = isHome ? "testdb" : "admin_project";
const dialect = "mysql";
const pool = {
  max: 5,
  min: 0,
  acquire: 30000,
  idle: 10000
}

export {
  HOST,
  USER,
  PASSWORD,
  DB,
  dialect,
  pool,
  raspisanie,
  raspisanieKursi
}
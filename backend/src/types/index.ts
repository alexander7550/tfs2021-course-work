/* eslint-disable @typescript-eslint/no-explicit-any */

export enum EStatusCode {
  OK = 'OK',
  MissingParam = 'MissingParam',
  InvalidParamValue = 'InvalidParamValue',
  MissingToken = 'MissingToken',
  InvalidToken = 'InvalidToken',
  EmailTaken = 'EmailTaken',
  
  UsernameTaken = 'UsernameTaken',
  InvalidUsernameOrPassword = 'InvalidUsernameOrPassword'
}

export interface RequestStatus {
  status_code: EStatusCode,
  result?: any,
  param?: string
}
